﻿#Creating CentOS Vagrant Box
>Created 20160603

The purpose of this build doc is to document the steps used to create a CentOS Vagrant box for local dev environments.

Instructions below are a derived from [this site](http://thornelabs.net/2013/11/11/create-a-centos-6-vagrant-base-box-from-scratch-using-virtualbox.html)

##Configuring the Host Machine
1. Install VirtualBox
2. Install Vagrant
3. Install VirtualBox extensions
4. Create new VM
	1. Name: “centos-6.x_x64”
	2. Version: Red Had (64bit)
	3. Memory: 2GB
	4. Create VDI Harddrive
		* 64GB, Dynamically allocated
	5. Mount the netinstall of CentOS 6.6
##Configuring the Guest Machine
1. Boot VM centos-6.x_x64
2. Install OS (Use defaults unless specified otherwise)
	1. Select URL Installation method
	2. Disable IPv6
	3. URL setup: http://mirror.centos.org/centos/6/os/x86_64
	4. Root Password: vagrant
	5. Select option for “Use All Space”
	6. Select option for “Web Server”
	7. After installation completes, remove CD image and reboot

3. Configure OS for Vagrant box build
	1. Update OS
		* `yum update -y`
	2. Reboot OS
	3. Login
	4. Install packages
		* `yum install -y openssh-clients man git vim wget curl ntp`
		* `yum install -y httpd-devel php-mysql php-devel php-soap php-pgsql php-xmlrpc`
		* `yum install -y php-pecl-apc php-pecl-memcache`
	5. Enable ntp
		* `chkconfig ntpd on`
	6. Set time 
		* `service ntpd stop`
		* `ntpdate 0.us.pool.ntp.org`
		* `service ntpd start`
	7. Enable the ssh service to start on boot:
		* `chkconfig sshd on`
	8. Install Guest Addtions into Guest
		* `yum install -y gcc kernel-devel make`
		* Insert Guest Additions CD via the Devices menu in VirtualBox
		* `mkdir -p /media/cdrom`
		* `mount /dev/scd0 /media/cdrom`
		* `sh /media/cdrom/VboxLinuxAdditions.run`
	9. Disable the iptables and ip6tables services from starting on boot:
		* `chkconfig iptables off`
		* `chkconfig ip6tables off`
	10. Set SELinux to permissive:
		* `sed -i -e 's/^SELINUX=.*/SELINUX=permissive/' /etc/selinux/config`
	11. Add vagrant user:
		* `useradd vagrant`
	12. Set password for the vagrant user to “vagrant”
		* `passwd vagrant`
	13. Accept the warnings for the weak password
	14. Create vagrant user’s .ssh folder:
		* `mkdir -m 0700 -p /home/vagrant/.ssh`
	15. Download vagrant SSH keys
		* `curl https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub >> /home/vagrant/.ssh/authorized_keys`
	16. Change permissions on authorized_keys files to be more restrictive:
		* `chmod 600 /home/vagrant/.ssh/authorized_keys`
	17. Make sure vagrant user and group owns the .ssh folder and its contents:
		* `chown -R vagrant:vagrant /home/vagrant/.ssh`
	18. Comment out requiretty in /etc/sudoers. This change is important because it allows ssh to send remote commands using sudo. Without this change, vagrant will be unable to apply changes (such as configuring additional NICs) at startup:
		* `echo "Defaults:vagrant !requiretty" >> /etc/sudoers`
	19. Allow user vagrant to use sudo without entering a password:
		* `echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers`
	20. Open /etc/sysconfig/network-scripts/ifcfg-eth0 and make it look like the following:
        * `DEVICE=eth0`
		* `TYPE=Ethernet`
		* `ONBOOT=yes`
		* `NM_CONTROLLED=no`
		* `BOOTPROTO=dhcp`
	21. Clean up installation
		1. Remove the udev persistent net rules file:
			* `rm -f /etc/udev/rules.d/70-persistent-net.rules`
		2. Clean up yum:
			* `yum clean all`
		3. Clean up the tmp directory:
			* `rm -rf /tmp/*`
		4. Clean up the last logged in users logs:
			* `rm -f /var/log/wtmp /var/log/btmp`
		5. Clean up history:
			* `history -c`
	22. Shutdown the virtual machine:
		* `shutdown -h now`
##Create and Test Vagrant Box
1. Open a terminal window on the host to the location of the centos-6.6-x86_64 virtual machine.
2. Create the Vagrant Box
	* `vagrant package --output centos-6.x_x64.box --base centos-6.x_x64`
3. Add the newly created Vagrant box to vagrant (this will copy the Vagrant box to a new location).
	* `vagrant box add centos-6.x_x64 centos-6.x_x64`
4. Optional: copy the Vagrant box to a reachable web host for other clients to download it via their vagrant configuration files
5. Create a vagrant project folder
	* `mkdir -p ~/projects/TestCentOS`
	* `cd ~/projects/TestCentOS`
	* `vagrant init centos-6.x_x64`
	* `vagrant up`