#!/usr/bin/env bash
# netbeans.sh
#
# Clean installs and configures Netbeans IDE inside the VM.

# Download and install Netbeans v8.2
  rm -rf /vagrant/apps/netbeans81
  mkdir /vagrant/apps -p
  cd /vagrant/apps
  if [ ! -f /vagrant/apps/netbeans.zip ]; then
    echo "Netbeans.zip file not found! Downloading from netbeans.org"
    wget http://download.netbeans.org/netbeans/8.1/final/zip/netbeans-8.1-201510222201.zip -O netbeans.zip
  fi
  unzip /vagrant/apps/netbeans.zip -d /vagrant/apps
  mv /vagrant/apps/netbeans /vagrant/apps/netbeans81
  chown vagrant:vagrant -R /vagrant/apps/netbeans81
  printf '\nalias netbeans="/vagrant/apps/netbeans81/bin/netbeans"' >> /home/vagrant/.bashrc

# Install Openjdk 1.8
  echo "Installing Openjdk 1.8 dependencies"
  yum install -y java-1.8.0-openjdk-devel

# Behat/ Selenium (system-wide installation)
  if [ ! -f /usr/bin/selenium ]; then
    echo "Installing Selenium and related tools"
      mkdir -p /vagrant/export/test-results
      cd /home/vagrant
      wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar
      mv /home/vagrant/selenium-server-standalone-2.53.0.jar /usr/bin/selenium
      yum install -y firefox Xvfb xorg-x11-fonts* xauth php-yaml
      printf '#!/bin/bash\n\nkillall java\nkillall xvfb-run\nkillall Xvfb\nkillall firefox\nrm /vagrant/export/selenium.log\n\nDISPLAY=localhost:10 xvfb-run java -jar /usr/bin/selenium > /vagrant/export/selenium.log&\n' > /home/vagrant/selenium-xvfb.sh
      chmod +x /home/vagrant/selenium-xvfb.sh
      chown vagrant:vagrant /home/vagrant/selenium-xvfb.sh
  fi

# Configure Netbeans
  echo "Pre-configuring Netbeans"
  rm -rf /vagrant/.netbeans
  rm -rf /vagrant/NetBeansProjects
  tar -xvzf /vagrant/buildFiles/configs/netbeansConfig.tar.gz -C /tmp
  chown -R vagrant:vagrant /tmp/netbeansConfig
  mv /tmp/netbeansConfig/.netbeans /home/vagrant/
  mv /tmp/netbeansConfig/NetBeansProjects /home/vagrant/
