#!/usr/bin/env bash
# cleanDrupal8Install.sh
#
# Drupal 8 provisioning script

# Clean up public_html directory
    echo "Cleaning up public_html directory..."
    cd /vagrant/public_html
    rm -rf /vagrant/public_html
    mkdir -p /vagrant/public_html
    cd /tmp
    cd /vagrant/public_html

# Check PHP version, upgrade if necessary
    VER=$(php -i | grep "Module version" | awk '{print $4}')
    MVER=$(echo $VER | cut -c1)
    if [ $MVER -ne 7 ]
        then
          echo "Installed PHP version ($VER) is too old."
          chmod +x /vagrant/bootstrapScripts/CentOS/upgradeLAMP.sh
          sh /vagrant/bootstrapScripts/CentOS/upgradeLAMP.sh
    fi

# Fix DocRoot for D8
    sed -i "s%DocumentRoot /var/www/html%DocumentRoot /var/www/html/web%" /etc/httpd/conf/httpd.conf
    sed -i 's%"/var/www/html"%"/var/www/html/web/"%'  /etc/httpd/conf/httpd.conf


# Install Drupal
    echo "Beginning Drupal Provisioning"
    cd /vagrant/public_html
    sudo -u vagrant /usr/local/bin/composer create-project drupal-composer/drupal-project:8.x-dev /vagrant/public_html --stability dev --no-interaction
    sudo -u vagrant /usr/local/bin/composer require drush/drush
    echo "Installing drush and drush launcher"
    rm /usr/local/bin/drush
    cd /vagrant/public_html
    wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.3.1/drush.phar
    chmod +x drush.phar
    sudo mv drush.phar /usr/local/bin/drush
    sudo -u vagrant /usr/local/bin/drush init -y
    cd /vagrant/public_html
    drush si --db-url=mysql://root:root@localhost/Web --account-name=vagrant --account-pass=vagrant --site-name=website.vbox.local -y

# Restart Services
    service httpd restart
    service mysqld restart

echo "Drupal installation ready! Login to http://website.vbox.local with vagrant:vagrant"