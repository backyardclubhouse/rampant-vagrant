#!/bin/bash
# developerSuite.sh
#
# Installs PHP CodeSniffer, Coder, Behat, Selenium, Drush, wp-cli, npm, jslint

# Install CLI tools
  wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  chmod +x wp-cli.phar
  mv wp-cli.phar /usr/local/bin/wp
  mkdir /home/vagrant/.drush/dump -p
  chown vagrant:vagrant /home/vagrant/.drush -R

# Install Xdebug/ XHProf
  yum install -y php-pecl-xdebug xhprof
  printf '\nxdebug.remote_enable = on\nxdebug.remote_connect_back = on\nxdebug.idekey = "vagrant"\nxdebug.port=9000' >> /etc/php.d/xdebug.ini

# Install ImageMagick
  yum install -y ImageMagick ImageMagick-perl

# Behat/ Selenium (system-wide installation)
  echo "Installing Selenium and related tools"
    mkdir -p /vagrant/export/test-results
    cd /home/vagrant
    wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar
    mv /home/vagrant/selenium-server-standalone-2.53.0.jar /usr/bin/selenium
    printf '\nalias selenium="java -jar /usr/bin/selenium"' >> /home/vagrant/.bashrc
    yum install -y firefox Xvfb xorg-x11-fonts* xauth php-yaml
    printf '#!/bin/bash\n\nkillall java\nkillall xvfb-run\nkillall Xvfb\nkillall firefox\nrm /vagrant/export/selenium.log\n\nDISPLAY=localhost:10 xvfb-run java -jar /usr/bin/selenium > /vagrant/export/selenium.log&\n' > /home/vagrant/selenium-xvfb.sh
    chmod +x /home/vagrant/selenium-xvfb.sh
    chown vagrant:vagrant /home/vagrant/selenium-xvfb.sh

  echo "Installing Drush, Behat, Coder, and PHP CodeSniffer"
    cd /home/vagrant
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    mkdir /opt/globalcomposer -p
    cd /opt/globalcomposer
    printf '{\n\t"require": {\n\t\t"drush/drush": "^7.3",\n\t\t"drupal/coder": "^8.2",\n\t\t"drupal/drupal-extension": "~3.0",\n\t\t"behat/mink": "1.5.*@stable",\n\t\t"behat/mink-goutte-driver": "*",\n\t\t"behat/mink-selenium2-driver": "*"\t\n\t},\n\t"config": {\n\t\t"bin-dir": "bin/"\n\t}\n}' > /opt/globalcomposer/composer.json
    cd /opt/globalcomposer
    php /usr/local/bin/composer install

# Create symlinks to applications
  ln -s /opt/globalcomposer/bin/behat /usr/local/bin/behat
  ln -s /opt/globalcomposer/bin/phpcs /usr/local/bin/phpcs
  ln -s /opt/globalcomposer/bin/phpcbf /usr/local/bin/phpcbf
  ln -s /opt/globalcomposer/bin/drush /usr/local/bin/drush

# Configure CodeSniffer
  echo "Configuring CodeSniffer"
    printf '\nalias drupalcs="phpcs --standard=Drupal --extensions='\''php,module,inc,install,test,profile,theme,js,css,info,txt'\''"' >> /home/vagrant/.bashrc
    printf '\nalias drupalcbf="phpcbf --standard=Drupal"' >> /home/vagrant/.bashrc
    printf '\nalias wpcs="phpcs --standard=Wordpress"' >> /home/vagrant/.bashrc
    printf '\nalias wpcbf="phpcbf --standard=Wordpress"' >> /home/vagrant/.bashrc
    phpcs --config-set installed_paths /opt/globalcomposer/vendor/drupal/coder/coder_sniffer

# Add global git color configuration
  printf '[color]\n\tui = true\n' >> /home/vagrant/.gitconfig
  chown vagrant:vagrant /home/vagrant/.gitconfig

# Install latest stable nodejs and npm
  cd /tmp
  curl -sL https://rpm.nodesource.com/setup_8.x | sudo -E bash -
  yum install -y nodejs

# Global npm modules
  npm install -g browser-sync
  npm install -g jslint

# Restart services
  service mysqld restart
  service httpd restart

echo "Developer Suite Toolsets installed"
